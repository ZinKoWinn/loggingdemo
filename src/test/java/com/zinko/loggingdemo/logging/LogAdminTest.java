package com.zinko.loggingdemo.logging;

import com.zinko.loggingdemo.logging.entity.User;
import com.zinko.loggingdemo.logging.repository.LogAdminRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)

public class LogAdminTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private LogAdminRepository logAdminRepository;
    @Autowired
    private JacksonTester<User> userJson;

    @Test
    @Order(1)
    @DisplayName("Logging create operation of an entity object")
    void createAction() throws Exception {
        User user = User.builder().name("Zin Ko Winn").password("zinowin").phoneNumber("09797256920").build();
        ResultActions response = mockMvc.perform(post("/api/admin/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson.write(user).getJson()));

        response.andExpect(status().isCreated()).andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is(user.getName())))
                .andExpect(jsonPath("$.password", is(user.getPassword())))
                .andExpect(jsonPath("$.phoneNumber", is(user.getPhoneNumber())));

        logAdminRepository.findAll().forEach(System.out::println);
    }

    @Test
    @Order(2)
    @Sql("classpath:insert_user.sql")
    @DisplayName("Logging update operation of an entity object")
    void updateAction() throws Exception {
        User user = User.builder().id(1L).name("Update Zin Ko Winn").password("zinowin").phoneNumber("09797256920").build();
        ResultActions response = mockMvc.perform(put("/api/admin/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userJson.write(user).getJson()));

        response.andExpect(status().isOk()).andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is(user.getName())))
                .andExpect(jsonPath("$.password", is(user.getPassword())))
                .andExpect(jsonPath("$.phoneNumber", is(user.getPhoneNumber())));

        logAdminRepository.findAll().forEach(System.out::println);
    }
}
