package com.zinko.loggingdemo.logging.repository;

import com.zinko.loggingdemo.logging.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
