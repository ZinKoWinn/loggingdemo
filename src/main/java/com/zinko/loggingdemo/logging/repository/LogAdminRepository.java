package com.zinko.loggingdemo.logging.repository;

import com.zinko.loggingdemo.logging.entity.LogAdmin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogAdminRepository extends JpaRepository<LogAdmin, Long> {
}
