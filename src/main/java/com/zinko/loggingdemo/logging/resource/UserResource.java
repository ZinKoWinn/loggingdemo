package com.zinko.loggingdemo.logging.resource;

import com.zinko.loggingdemo.logging.annotation.Monitor;
import com.zinko.loggingdemo.logging.entity.User;
import com.zinko.loggingdemo.logging.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/admin")
public class UserResource {
    private final UserService userService;

    @Monitor(description = "Create")
    @PostMapping("/user")
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@RequestBody User user) {
        return userService.create(user);
    }

    @Monitor(description = "Update")
    @PutMapping("/user")
    public User updateUser(@RequestBody User user) {
        return userService.update(user);
    }
}
