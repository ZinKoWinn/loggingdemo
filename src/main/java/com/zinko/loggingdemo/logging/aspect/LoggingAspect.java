package com.zinko.loggingdemo.logging.aspect;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zinko.loggingdemo.logging.entity.LogAdmin;
import com.zinko.loggingdemo.logging.repository.LogAdminRepository;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.lang.reflect.Field;

@Component
@Aspect
@RequiredArgsConstructor
public class LoggingAspect {

    private static final String FIELD_NAME_ID = "id";
    private final ObjectMapper objectMapper;
    private final EntityManager entityManager;
    private final LogAdminRepository logAdminRepository;

    private static final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Around("@annotation(com.zinko.loggingdemo.logging.annotation.Monitor)")
    public Object logEntityAction(ProceedingJoinPoint joinPoint) throws Throwable {
        logger.info("LoggingAspect.logEntityAction called");
        try {
            LogAdmin logAdmin = createLogAdmin(joinPoint);
            logAdminRepository.save(logAdmin);
        } catch (Exception e) {
            logger.error("LoggingAspect.logEntityAction error", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Unable to log operation");
        }
        logger.info("LoggingAspect.logEntityAction finished");
        return joinPoint.proceed();
    }

    private LogAdmin createLogAdmin(ProceedingJoinPoint joinPoint) throws Throwable {
        LogAdmin logAdmin = new LogAdmin();
        Object entityObj = joinPoint.getArgs()[0];
        Field idField = entityObj.getClass().getDeclaredField(FIELD_NAME_ID);
        idField.setAccessible(true);
        Number idValue = (Number) idField.get(entityObj);
        if (idValue == null || idValue.longValue() <= 0) {
            logAdmin.setAction("Create");
            logAdmin.setOldValue("null");
            joinPoint.proceed();
            Object generatedId = getLatestId(entityObj.getClass().getName());
            logAdmin.setNewValue(objectMapper.writeValueAsString(entityManager.find(entityObj.getClass(), generatedId)));
        } else {
            Object oldValue = entityManager.find(entityObj.getClass(), idValue.longValue());
            logAdmin.setAction("Update");
            logAdmin.setNewValue(objectMapper.writeValueAsString(entityObj));
            logAdmin.setOldValue(objectMapper.writeValueAsString(oldValue));
        }
        return logAdmin;
    }

    private Number getLatestId(String tableName) {
        return (Number) entityManager.createQuery("SELECT MAX(id) FROM ".concat(tableName))
                .getSingleResult();
    }
}
